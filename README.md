## S98Pro-user 12 SP1A.210812.016 1668499904 release-keys
- Manufacturer: doogee
- Platform: mt6781
- Codename: S98Pro
- Brand: DOOGEE
- Flavor: sys_mssi_64_ww-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: 1668499904
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: ru-RU
- Screen Density: undefined
- Fingerprint: DOOGEE/S98Pro_RU/S98Pro:12/SP1A.210812.016/1668499904:user/release-keys
- OTA version: 
- Branch: S98Pro-user-12-SP1A.210812.016-1668499904-release-keys
- Repo: doogee_s98pro_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
